LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

# prebuilt libSoliCallSDK-android.a
include $(CLEAR_VARS)
LOCAL_MODULE          	:= libSoliCallSDK
LOCAL_PREBUILT_PREFIX   := lib
LOCAL_PREBUILT_SUFFIX   := .a
LOCAL_SRC_FILES       	:= libSoliCallSDK-android.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE    := native
LOCAL_SRC_FILES := native.cpp
LOCAL_LDLIBS := -llog
LOCAL_STATIC_LIBRARIES := libSoliCallSDK  
LOCAL_CFLAGS    := -D_LINUX
include $(BUILD_SHARED_LIBRARY)
