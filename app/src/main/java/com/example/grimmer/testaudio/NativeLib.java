package com.example.grimmer.testaudio;

/**
 * Created by grimmer on 2015/6/30.
 */
public class NativeLib {
    static{
        System.loadLibrary("native");
    }

    public native int getHelloMessage();

    //public native int packageInit();

}