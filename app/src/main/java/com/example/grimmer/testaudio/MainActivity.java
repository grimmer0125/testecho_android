package com.example.grimmer.testaudio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.media.audiofx.AcousticEchoCanceler;

import android.util.Log;


public class MainActivity extends AppCompatActivity implements OnClickListener {

    private RecordThread mRecordThread1;
    private PlayThread mPlayThread1;

    private RecordThread mRecordThread2;
    private PlayThread mPlayThread2;

    private Button recordButton;

    private Button record_play_Button;

    private Button playButton;

    private Switch webRTCSwitch;

    boolean startRecord =false;
    boolean startRecord_play =false;
    boolean startPlay = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recordButton = (Button)this.findViewById(R.id.recordbutton);
        recordButton.setOnClickListener(this);

        record_play_Button = (Button)this.findViewById(R.id.record_play_button);
        record_play_Button.setOnClickListener(this);

        playButton = (Button)this.findViewById(R.id.play_button);
        playButton.setOnClickListener(this);

        webRTCSwitch =(Switch)this.findViewById(R.id.switch1);

//        mPlay1 =(Button)this.findViewById(R.id.bt_play1);

        if (AcousticEchoCanceler.isAvailable()) {
            //webRTCSwitch.setClickable(true);
            webRTCSwitch.setChecked(true);
        }
        else
        {
            webRTCSwitch.setChecked(false);
            webRTCSwitch.setClickable(false);
            webRTCSwitch.setText("No native AEC");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.recordbutton: {

                int v2= 0;

                NativeLib lib = new NativeLib();


                int v33 =  lib.getHelloMessage(); //Native_solLib.getHelloMessage();
                //Log.d("", str);


                if (startRecord)
                {
                    mRecordThread1.StopRecord();

                    startRecord=false;
                    recordButton.setText("Record");

                }
                else
                {
                    startRecord=true;
                    recordButton.setText("Recording");

                    mRecordThread1 = new RecordThread();
                    mRecordThread1.StartRecordWithoutAEC();
                }
                break;
            }
            case R.id.record_play_button: {

                if (startRecord_play)
                {
                    mRecordThread2.StopRecord();

                    startRecord_play=false;
                    record_play_Button.setText("Record_play");

                }
                else
                {
                    mRecordThread2 = new RecordThread();
                    mRecordThread2.StartRecord(webRTCSwitch.isChecked());

                    mPlayThread1 = new PlayThread();
                    mPlayThread1.StartPlay(mRecordThread1.getBufferList());

                    startRecord_play=true;
                    record_play_Button.setText("Record_playing");
                }
                break;
            }
            case R.id.play_button: {

                if (startPlay)
                {
                    startPlay=false;
                    playButton.setText("play");

                }
                else
                {
                    mPlayThread2 = new PlayThread();
                    mPlayThread2.StartPlay(mRecordThread2.getBufferList());

                    startPlay=true;
                    playButton.setText("playing");
                }
                break;
            }

        }
    }
}
